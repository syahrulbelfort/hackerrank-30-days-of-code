function solve(meal_cost, tax_percent, tip_percent){
    let tip = meal_cost * (tip_percent/100);
    let tax = meal_cost * (tax_percent/100);
    let totalCost = tip + tax + meal_cost
    return Math.round(totalCost)
}

console.log(solve(100,15,10))