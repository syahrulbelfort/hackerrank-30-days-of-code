function reverseArr() {
    const n = parseInt(readLine().trim(), 10);

    const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));
    
   const reverseArr =  arr.reverse()
    
    console.log(reverseArr.join(' ')) 
}
